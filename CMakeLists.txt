cmake_minimum_required(VERSION 2.8)

# main project name defined
project(near-tools)

# osx architectures
set(CMAKE_OSX_ARCHITECTURES "x86_64")

# output directories
if(CMAKE_GENERATOR STREQUAL "Unix Makefiles")
    #set(BIN_DIR_SUFFIX mk)
    set(MAKEFILES ON)
    set(CMAKE_VERBOSE_MAKEFILE ${BUILD_VERBOSE})
elseif(CMAKE_GENERATOR STREQUAL "Xcode")
    #set(BIN_DIR_SUFFIX xcode)
    set(XCODE ON)
elseif(CMAKE_GENERATOR STREQUAL "Visual Studio 10 Win64")
	add_definitions(-D_SCL_SECURE_NO_WARNINGS)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif(CMAKE_GENERATOR STREQUAL "Unix Makefiles")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin/${BIN_DIR_SUFFIX})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin/${BIN_DIR_SUFFIX})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin/${BIN_DIR_SUFFIX})

#include directories
include_directories(${PROJECT_SOURCE_DIR}/src)
include_directories(${PROJECT_SOURCE_DIR}/src/common)
include_directories(/usr/local/include)
include_directories(/usr/include)

# libraries
find_package(OpenGL)
find_package(GLUT)

if(APPLE)
    find_library(TBB tbb /usr/local/lib)
    find_library(TBB_MALLOC tbbmalloc /usr/local/lib)
endif()

if(WIN32)
    GET_FILENAME_COMPONENT(GL_PATH ../OpenGL ABSOLUTE)
    include(${GL_PATH}/use_gl.cmake)
    GET_FILENAME_COMPONENT(BOOST_PATH ../boost ABSOLUTE)
    include(${BOOST_PATH}/use_boost.cmake)
    GET_FILENAME_COMPONENT(TBB_PATH ../tbb ABSOLUTE)
    include(${TBB_PATH}/use_tbb.cmake)
endif()

if(UNIX AND NOT APPLE)
    find_library(TBB tbb /usr/lib64)
    find_library(TBB_MALLOC tbbmalloc /usr/lib64)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lrt")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lGLU")
endif()

message("${GLUT_glut_LIBRARY}")

add_definitions(-DBOOST_ALL_NO_LIB)

# set C++11
if(CMAKE_GENERATOR STREQUAL "Xcode")
    set(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++11")
    set(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++")
else(CMAKE_GENERATOR STREQUAL "Xcode")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    if(APPLE)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
    endif()
endif(CMAKE_GENERATOR STREQUAL "Xcode")

if(WIN32)
    add_definitions(-DNOMINMAX)
endif()

# set default build to release
set(CMAKE_BUILD_TYPE Release)

add_subdirectory(src)
