#!/usr/bin/python

import os
import glob
import sys


def convert_images(path):
    path = os.path.normpath(path)
    json_path = os.path.normpath(os.path.join(path, '*.[Jj][Ss][Oo][Nn]'))
    gp = glob.glob(json_path)

    for fname in gp:
        if os.path.isfile(fname):
            cmd = "j2b " + fname + " " + fname
            print cmd
            os.system(cmd)

def main(argv = None):
    if argv == None or len(argv) <= 1:
        path = ''
    else:
        path = sys.argv[1]
    convert_images(path)

if __name__ == '__main__':
    main(sys.argv)