#include <common/debug.h>
#include <common/stddir.h>
#include <common/json.h>
#include <common/binio.h>
#include <scene/scene.h>
#include <common/stdheader.h>

std::string ifilename;
std::string ofilename;

bool parseCmdline(int argc, char** argv);
int main(int argc, char** argv) {
    if (parseCmdline(argc, argv) != true)
        return 0;
    
    jsonObject json = loadJson(ifilename).as_object_ref();
    std::map<std::string, jsonObject> reflectances;
    std::map<std::string, jsonObject> emissions;
    
    const auto &jrefl = jsonGet(json, "reflectances").as_array_ref();
    const auto &jemit = jsonGet(json, "emissions").as_array_ref();
    const auto &jmat = jsonGet(json, "materials").as_array_ref();
    
    for (const auto& r : jemit)
        emissions[jsonGet(r, "name").as_string()] = r.as_object_ref();
    
    for (const auto& r : jrefl)
        reflectances[jsonGet(r, "name").as_string()] = r.as_object_ref();
    
    jsonRemove(json, "reflectances");
    jsonRemove(json, "emissions");
    
    jsonArray njmat;
    for (const auto &m : jmat) {
        jsonObject jm;
        if (jsonHas(m, "opacity_ref")) jm["opacity_ref"] = jsonGet(m, "opacity_ref");
        if (jsonHas(m, "bump_ref")) jm["bump_ref"] = jsonGet(m, "bump_ref");
        jm["name"] = jsonGet(m, "name");
        
        if (jsonHas(m, "emission_ref")) { // process emission
            jsonObject e = emissions[jsonGet(m, "emission_ref").as_string()];
            if (jsonGet(e, "type").as_string() == "lambertemission") {
                jm["type"] = "lambertemissionmaterial";
                jm["le"] = jsonGet(e, "le");
                if(jsonHas(e, "letxt_ref")) jm["letxt_ref"] = jsonGet(e, "letxt_ref");
            } else error("unknown emission type");
        } else if (jsonHas(m, "reflectance_ref")) { // process reflection
            jsonObject r = reflectances[jsonGet(m, "reflectance_ref").as_string()];
            if (jsonGet(r, "type").as_string() == "lambertreflectance") {
                jm["type"] = "lambertmaterial";
                jm["rhod"] = jsonGet(r, "rhod");
                if(jsonHas(r, "rhotxt_ref")) jm["rhotxt_ref"] = jsonGet(r, "rhotxt_ref");
            } else if (jsonGet(r, "type").as_string() == "microfacetreflectance") {
                jm["type"] = "microfacetmaterial";
                jm["rhod"] = jsonGet(r, "rhod");
                if(jsonHas(r, "rhodtxt_ref")) jm["rhodtxt_ref"] = jsonGet(r, "rhodtxt_ref");
                jm["rhos"] = jsonGet(r, "rhos");
                if(jsonHas(r, "rhostxt_ref")) jm["rhostxt_ref"] = jsonGet(r, "rhostxt_ref");
                jm["n"] = jsonGet(r, "n");
                if(jsonHas(r, "ntxt_ref")) jm["ntxt_ref"] = jsonGet(r, "ntxt_ref");
                jm["diffuse"] = jsonGet(r, "diffuse");
                jm["specular"] = jsonGet(r, "specular");
            } else if (jsonGet(r, "type").as_string() == "kajiyahairreflectance") {
                jm["type"] = "kajiyahairmaterial";
                jm["rhod"] = jsonGet(r, "rhod");
                if(jsonHas(r, "rhotxt_ref")) jm["rhotxt_ref"] = jsonGet(r, "rhotxt_ref");
            } else error("unknown emission type");
        }
        njmat.push_back(jm);
    }
    jsonSet(json, "materials", njmat);
    saveJson(ofilename, json);
}


bool parseCmdline(int argc, char** argv) {
    const char *usage = "er2m scene[.json|.jbin]";
    if (argc < 2) { message(usage); return false; }
    ifilename = argv[1];
    std::string ext = getFileExtension(ifilename);
    if (argc == 2) {
        if (ext == "json") {
            ofilename = getFileBaseName(ifilename) + ".jbin";
        } else if (ext == "jbin") {
            ofilename = getFileBaseName(ifilename) + ".json";
        }
    } else {
        ofilename = argv[2];
    }
    return true;
}
