#include <common/debug.h>
#include <common/stddir.h>
#include <common/json.h>
#include <common/binio.h>
#include <string>
using std::vector;

std::string ifilename;
std::string ofilename;

bool parseCmdline(int argc, char** argv);
int main(int argc, char** argv) {
    if (parseCmdline(argc, argv) != true)
        return 0;
    
    jsonValue json;
    std::string ext = getFileExtension(ifilename);
    if (ext == "json") {
        json = loadJson(ifilename);
    } else if (ext == "jbin") {
        json = loadBin(ifilename);
    } else {
        error("unknown file extension.");
    }
    
    ext = getFileExtension(ofilename);
    if (ext == "json") {
        saveJson(ofilename, json);
    } else if (ext == "jbin") {
        saveBin(ofilename, json);
    } else {
        error("unknown file extension.");
    }
}


bool parseCmdline(int argc, char** argv) {
    const char *usage = "j2b scene[.json|.jbin]";
    if (argc < 2) { message(usage); return false; }
    ifilename = argv[1];
    std::string ext = getFileExtension(ifilename);
    if (argc == 2) {
        if (ext == "json") {
            ofilename = getFileBaseName(ifilename) + ".jbin";
        } else if (ext == "jbin") {
            ofilename = getFileBaseName(ifilename) + ".json";
        }
    } else {
        ofilename = argv[2];
    }
    return true;
}
