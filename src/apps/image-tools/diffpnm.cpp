#include <common/pam.h>

enum diffMode { L2_ERROR, L1_ERROR, PERCENTAGE_ERROR };
Image<float> computeDifferenceImage(const Image<vec3f> &image1, const Image<vec3f> &image2, int errMode) {
    error_if(image1.width() != image2.width() || image1.height() != image2.height(),
             "image dimension do not match");
    Image<float> diff(image1.width(), image1.height());
    if (errMode == L1_ERROR) {
        for(int i = 0; i < diff.size(); i ++) {
            const vec3f &i0 = image1.at(i);
            const vec3f &i1 = image2.at(i);
            vec3f d = abs_component(i0 - i1);
            diff.at(i) = d.average();
        }
    } else if (errMode == L2_ERROR) {
        for(int i = 0; i < diff.size(); i ++) {
            const vec3f &i0 = image1.at(i);
            const vec3f &i1 = image2.at(i);
            vec3f d = abs_component(i0 - i1);
            diff.at(i) = d.length();
        }
    } else if (errMode == PERCENTAGE_ERROR) {
        for(int i = 0; i < diff.size(); i ++) {
            const vec3f &i0 = image1.at(i);
            const vec3f &i1 = image2.at(i);
            vec3f d = abs_component(i0 - i1);
            d /= i0;
            diff.at(i) = d.average();
        }
    }
    return diff;
}

double imageSum(const Image<float> &img) {
    double sum = 0.0;
    for (int i = 0; i < img.size(); i++) {
        sum += img.at(i);
    }
    return sum;
}

int main(int argc, char** argv) {
    const char* usage = "diffpnm reference.pfm image.pfm diff.pfm -[L1|L2|PC]";
    if (argc < 4) {
        error(usage);
        return 0;
    }
    string filename1 = argv[1];
    string filename2 = argv[2];
    string outputfilename = argv[3];
    
    Image<vec3f> image1 = loadPnm3f(filename1);
    Image<vec3f> image2 = loadPnm3f(filename2);
    
    int mode = L1_ERROR;
    if (argc >= 5) {
        string opt = argv[4];
        if (opt == "-L1" || opt == "-l1")
            mode = L1_ERROR;
        else if (opt == "-L2" || opt == "-l2")
            mode = L2_ERROR;
        else
            mode = PERCENTAGE_ERROR;
    }

    Image<float> diff = computeDifferenceImage(image1, image2, mode);
    // average error
    double error = imageSum(diff) / diff.size();
    message_va("%lf", error);
    savePfm(outputfilename, diff);
    return 0;
}
