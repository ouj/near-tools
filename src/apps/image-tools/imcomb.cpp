#include <common/pam.h>
#include <common/debug.h>

string image_name;

int main(int argc, char** argv) {
    const char* usage = "usage: imcomb [image_name]";
    error_if_not(argc >= 2, usage);
    if(argc < 2) return false;
    
    image_name = argv[1];
    const int width = 512;
    const int height = 512;
    sarray2<vec3f> final(width, height);
    
    for (int j = 0; j < height; j++) {
        char line[16];
        sprintf(line, "%03d", j);
        string pfm_name = image_name + "_" + line + ".pfm";
        
        FILE *f = fopen(pfm_name.c_str(), "rb");
        if (f != 0) {
            fclose(f);
            sarray2<vec3f> image = loadPnm3f(pfm_name);
            for (int i = 0; i < width; i++)
                final.at(i, j) = image.at(i, j);
            message_va("%s processed", pfm_name.c_str());
        } else {
            message_va("%s missing", pfm_name.c_str());
        }
    }
    
    savePfm(image_name+".pfm", final);
}
