#include <common/pam.h>
#include <common/func.h>

Image<vec3f> computeExposure(Image<float> &diff, float exposureVal) {
    Image<vec3f> output(diff.width(), diff.height());
    float scale = pow(2, -exposureVal);
    for(int i = 0; i < diff.size(); i ++) {
        float l = diff[i] / scale;
        float v = max(0.0f, min(1.0f, l));
        float h = (1 - v) * 240.0;
        vec3f c = HsvToRgb(h, 1.0f, 1.0f);
        output.at(i) = c;
    }
    return output;
}

int main(int argc, char** argv) {
    const char* usage = "s2h diff.pfm heatmap.pfm [multiplier]";
    if (argc < 3) {
        error(usage);
        return 0;
    }
    string difffilename = argv[1];
    string heatfilename = argv[2];
    
    Image<float> diff = loadPnmf(difffilename);
    
    float exposureVal = 0.0f;
    if (argc >= 4) {
        sscanf(argv[4], "%f", &exposureVal);
    }
    
    sarray2<vec3f> heat = computeExposure(diff, exposureVal);
    savePfm(heatfilename, heat);
    return 0;
}
