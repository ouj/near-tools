#include <common/pam.h>
#include <scene/scene.h>
#include <scene/sceneio.h>
#include <scene/transform.h>
#include <vector>
#include <common/debug.h>
#include <common/quaternion.h>
using std::vector;

string scene_filename;
string ani_filename;

Scene* scene = 0;

template <typename R>
struct rotation {
    vec3<R>     axis;
    R           angle;
};

template<typename R>
struct animation {
    string          name;
    vec3<R>         traslation[2];
    rotation<R>     rot[2];
    int             frames;
};

typedef rotation<float> rotationf;
typedef animation<float> animationf;

rotationf parseRotation(const jsonObject &json) {
    rotationf r;
    r.axis = jsonGet(json, "axis").as_vec3f();
    r.angle = jsonGet(json, "angle").as_float();
    return r;
}

jsonObject printRotation(const rotationf &r) {
    jsonObject json;
    json["axis"] = r.axis;
    json["angle"] = r.angle;
    return json;
}

animationf parseAnimation(const jsonObject &json) {
    animationf ani;
    ani.name = jsonGet(json, "name").as_string();
    ani.traslation[0] = jsonGet(json, "translation0").as_vec3f();
    ani.traslation[1] = jsonGet(json, "translation1").as_vec3f();
    ani.rot[0] = parseRotation(jsonGet(json, "rotation0").as_object());
    ani.rot[1] = parseRotation(jsonGet(json, "rotation1").as_object());
    ani.frames = jsonGet(json, "frames").as_int();
    return ani;
}

jsonObject printAnimation(const animationf &animation) {
    jsonObject json;
    json["name"] = animation.name;
    json["translation0"] = animation.traslation[0];
    json["translation1"] = animation.traslation[1];
    json["rotation0"] = printRotation(animation.rot[0]);
    json["rotation1"] = printRotation(animation.rot[1]);
    json["frames"] = animation.frames;
    return json;
}

frame3f toRigidFrame(const animationf& ani, float t) {
    vec3f translation = lerp(ani.traslation[0], ani.traslation[1], t);
    quaternionf rot0(ani.rot[0].axis, radian(ani.rot[0].angle));
    quaternionf rot1(ani.rot[1].axis, radian(ani.rot[1].angle));
    quaternionf rot = slerp(t, rot0, rot1);
    frame3f frame = rot.toframe();
    frame.o = translation;
    return frame;
}

int main(int argc, char** argv) {
    const char* usage = "usage: animatize scene.json ani.json";
    error_if_not(argc >= 3, usage);
    if(argc < 3) return false;
    
    scene_filename = argv[1];
    ani_filename = argv[2];
    
    scene = loadScene(scene_filename);
    animationf ani = parseAnimation(loadJson(ani_filename));
    message(printJson(printAnimation(ani)).c_str());
    
    vector<Transform*> &transforms = scene->transforms;
    string scenename = scene_filename.substr(0, scene_filename.size() - 5);
    for (int t = 0; t < transforms.size(); t++) {
        Transform* transform = transforms[t];
        if (transform->name == ani.name && 
            transform->type == RigidTransform::TYPEID) {
            RigidTransform* rigid = (RigidTransform*)transform;
            for (int f = 0; f < ani.frames; f++) {
                rigid->frame = toRigidFrame(ani, f / (float)ani.frames);
                char findex[16]; sprintf(findex, "%d", f);
                string scene_output = scenename + findex + ".json";
                message(scene_output.c_str());
                SceneWriteOptions opts;
                opts.imageMode = SceneWriteOptions::IMAGEMODE_NONE;
                opts.meshMode = SceneWriteOptions::MESHMODE_INLINE;
                saveScene(scene_output, scene, opts);
            }
        }
    }
}
