#include <common/pam.h>
#include <scene/scene.h>
#include <scene/shape.h>
#include <scene/sceneio.h>
#include <scene/transform.h>
#include <vector>
#include <common/debug.h>
#include <common/quaternion.h>
using std::vector;

struct TerrainConfig {
    float zscale;
    int xresolution;
    int yresolution;
    float xscale;
    float yscale;
    vec3f center;
    string shapename;
    string geofolder;
};

TerrainConfig parseTerrainConfig(const jsonObject &json) {
    TerrainConfig config;
    config.zscale = jsonGet(json, "zscale");
    config.xscale = jsonGet(json, "xscale");
    config.yscale = jsonGet(json, "yscale");
    config.xresolution = jsonGet(json, "xresolution");
    config.yresolution = jsonGet(json, "yresolution");
    config.center = jsonGet(json, "center").as_vec3f();
    config.shapename = jsonGet(json, "shapename").as_string();
    return config;
};

float sampleImage(const sarray2<float> &image, const vec2f &uv) {
    int w = image.width();
    int h = image.height();
    int i = uv.x * w;
    int j = uv.y * h;
    return image.at(i, h-j-1);
}

jsonObject printShape(const Shape* val, 
                     const SceneWriteOptions::MeshMode& meshMode);
void generateTerrain(vector<vec3i> &faces, vector<vec3f> &pos,
                     vector<vec3f> &norms, vector<vec2f> &uvs,
                     const sarray2<float> &hm,
                     const TerrainConfig &config);

int main(int argc, char** argv) {
    const char* usage = "usage: terrain heightmap.pfm shape.json config.json";
    error_if_not(argc >= 4, usage);
    if(argc < 4) return false;
    
    string heightmap_filename = argv[1];
    string scene_filename = argv[2];
    string config_filename = argv[3];
    
    TerrainConfig config = parseTerrainConfig(loadJson(config_filename));
    sarray2<float> heightmap = loadPnmf(heightmap_filename);
    
    TriangleMeshShape *shape = new TriangleMeshShape();
    shape->name = config.shapename;
    shape->pos_filename = config.shapename + "_position";
    shape->face_filename = config.shapename + "_face";
    shape->uv_filename = config.shapename + "_texcoord";
    shape->norm_filename = config.shapename + "_normal";
    generateTerrain(shape->face, shape->pos, shape->norm, shape->uv, heightmap, config);
    jsonObject json = printShape(shape, SceneWriteOptions::MESHMODE_BINARY);
    jsonObject jsonScene;
    jsonScene["shapes"] = jsonArray(1, json);
    saveJson(scene_filename, jsonScene);
}

void generateTerrain(vector<vec3i> &faces, vector<vec3f> &pos,
                     vector<vec3f> &norms, vector<vec2f> &uvs,
                     const sarray2<float> &hm,
                     const TerrainConfig &config) {
    int w = config.xresolution * 0.5f;
    int h = config.yresolution * 0.5f;
    
    float xs = config.xscale / config.xresolution;
    float ys = config.yscale / config.yresolution;
    
    int npos = config.xresolution * config.yresolution;
    pos.resize(npos);
    uvs.resize(npos);
    for (int j = 0; j < config.yresolution; j++) {
        for (int i = 0; i < config.xresolution; i++) {
            vec2f uv = vec2f((float)i / config.xresolution,
                                 (float)j / config.yresolution);
            float z = sampleImage(hm, uv) * config.zscale;
            float x = (i - w) * xs;
            float y = (j - h) * ys;
            pos.at(i + j * config.xresolution) = vec3f(x, y, z) + config.center;
            uvs.at(i + j * config.xresolution) = uv;
        }
    }
    
    norms.resize(pos.size());
    for (int j = 0; j < config.yresolution; j++) {
        for (int i = 0; i < config.xresolution; i++) {
            const vec3f &p = pos.at(i + j * config.xresolution);
            const vec3f &x1 = (i == 0) ? p : pos.at(i-1 + j * config.xresolution);
            const vec3f &x2 = (i == config.xresolution - 1) ? p : pos.at(i+1 + j * config.xresolution);
            const vec3f &y1 = (j == 0) ? p : pos.at(i + (j-1) * config.xresolution);
            const vec3f &y2 = (j == config.yresolution - 1) ? p : pos.at(i + (j+1) * config.xresolution);
            
            vec3f v1 = x2 - x1;
            vec3f v2 = y2 - y1;
            norms.at(i + j * config.xresolution) = normalize(cross(v1, v2));
        }
    }
    
    int ntri = (config.xresolution - 1) * (config.yresolution - 1) * 2;
    faces.resize(ntri);
    int k = 0;
    for (int j = 0; j < config.yresolution - 1; j++) {
        for (int i = 0; i < config.xresolution - 1; i++) {
            int i0 = j * config.xresolution + i;
            int i1 = j * config.xresolution + i + 1;
            int i2 = (j + 1) * config.xresolution + i;
            int i3 = (j + 1) * config.xresolution + i + 1;
            faces.at(k++) = vec3i(i0, i2, i1);
            faces.at(k++) = vec3i(i1, i2, i3);
        }
    }
}


jsonObject printShape(const Shape* val, const SceneWriteOptions::MeshMode& meshMode) {
    jsonObject json;
    if(val->type == TriangleMeshShape::TYPEID) {
        const TriangleMeshShape* ptr = (const TriangleMeshShape*)val;
        json["type"] = "trianglemeshshape";
        json["name"] = ptr->name;
        if (meshMode == SceneWriteOptions::MESHMODE_INLINE) {
            json["face"] = printVec3iArray(ptr->face);
            json["pos"] = printVec3fArray(ptr->pos);
            if(!ptr->norm.empty()) json["norm"] = printVec3fArray(ptr->norm);
            if(!ptr->uv.empty()) json["uv"] = printVec2fArray(ptr->uv);
        } else {
#ifdef KEEP_SCENE_OBJ_FILENAME
            if(ptr->face_filename.empty()) json["face"] = printVec3iArray(ptr->face);
            else json["face_filename"] =  ptr->face_filename;
            if(ptr->pos_filename.empty()) json["pos"] = printVec3fArray(ptr->pos);
            else json["pos_filename"] =  ptr->pos_filename;
            if(!ptr->norm.empty()) {
                if(ptr->norm_filename.empty()) json["norm"] = printVec3fArray(ptr->norm);
                else json["norm_filename"] = ptr->norm_filename;
            }
            if(!ptr->uv.empty()) { 
                if(ptr->uv_filename.empty()) printVec2fArray(ptr->uv);
                else json["uv_filename"] = ptr->uv_filename;
            }
            if (meshMode != SceneWriteOptions::MESHMODE_FILENAMEONLY) {
                if(jsonHas(json, "face_filename")) saveRaw(json["face_filename"].as_string(), ptr->face);
                if(jsonHas(json, "pos_filename")) saveRaw(json["pos_filename"].as_string(), ptr->pos);
                if(!ptr->norm.empty() && jsonHas(json, "norm_filename")) 
                    saveRaw(json["norm_filename"].as_string(), ptr->norm);
                if(!ptr->uv.empty() && jsonHas(json, "uv_filename")) 
                    saveRaw(json["uv_filename"].as_string(), ptr->uv);
            }
#else
            const string &name = val->name;
            json["face_filename"] = name+".face";
            json["pos_filename"] = name+".pos";
            if(!ptr->norm.empty()) json["norm_filename"] = name+".norm";
            if(!ptr->uv.empty()) json["uv_filename"] = name+".texcoord";
            if (opts.meshMode != SceneWriteOptions::MESHMODE_FILENAMEONLY) {
                saveRaw(name+".face", ptr->face);
                saveRaw(name+".pos", ptr->pos);
                if(!ptr->norm.empty()) saveRaw(name+".norm", ptr->norm);
                if(!ptr->uv.empty()) saveRaw(name+".texcoord", ptr->uv);
            }
#endif
        }
    } else 
        error("unknown shape type");
    return json;
}

