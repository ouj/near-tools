#include <common/pam.h>
#include <common/progress.h>
#include <common/stdheader.h>
#include <json.h>
#include <debug.h>
#include <scene/scene.h>
#include <scene/sceneio.h>
#include <scene/shape.h>
#include <scene/transform.h>

string scenefilename = "scene.json";
Scene *scene = 0;

std::tuple<uint64_t, uint64_t, uint64_t>
computeTriangleNumbers(std::vector<Shape*> &shapes) {
    uint64_t mint = std::numeric_limits<uint64_t>::max();
    uint64_t maxt = 0;
    uint64_t total = 0;
    for (int s = 0; s < shapes.size(); s++) {
        Shape *shape = shapes[s];
        int count = triangleNum(shape);
        mint = min<uint64_t>(mint, count);
        maxt = max<uint64_t>(maxt, count);
        total += count;
    }
    return std::tuple<uint64_t, uint64_t, uint64_t> { mint, maxt, total };
};

struct BuildItem {
    range3f bbox;
    vec3f   centroid;
    int     index;
};

using biterator = std::vector<BuildItem>::iterator;
template<typename F>
void splitShape(biterator begin, biterator end, int maxtriangles,
                const F &makeShape) {
    error_if(end - begin == 0, "invalid size");
    
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;
    std::for_each(begin, end, [&bbox, &cbbox](const BuildItem &p) {
        bbox.grow(p.bbox);
        cbbox.grow(p.centroid);
    });
    
    int dim = cbbox.extent().max_component_index();
    if (cbbox.max[dim] == cbbox.min[dim] || (end - begin) <= maxtriangles) {
        return makeShape(begin, end);
    } else {
        float pmid = (cbbox.min[dim] + cbbox.max[dim]) * 0.5f;
        auto mid = std::partition(begin, end, [dim, pmid](const BuildItem& a) {
            return a.centroid[dim] < pmid;
        });
        if (mid == begin || mid == end) return makeShape(begin, end);
        else {
            splitShape(begin, mid, maxtriangles, makeShape);
            splitShape(mid, end, maxtriangles, makeShape);
        }
    }
}

std::vector<Shape*> splitShape(Shape *shape, int maxtriangles) {
    if (shape->type != TriangleMeshShape::TYPEID)
        return std::vector<Shape*> { new Shape(*shape) };
    load(shape);
    TriangleMeshShape *mesh = (TriangleMeshShape*)shape;
    std::vector<Shape*> sshapes;
    std::vector<BuildItem> items(mesh->face.size());
    for (int f = 0; f < mesh->face.size(); f++) {
        const vec3i &face = mesh->face[f];
        const vec3f &v0 = mesh->pos[face.x];
        const vec3f &v1 = mesh->pos[face.y];
        const vec3f &v2 = mesh->pos[face.z];
        items[f].bbox = triangleBoundingBox(v0, v1, v2);
        items[f].index = f;
        items[f].centroid = items[f].bbox.center();
    }
    
    int index = 0;
    auto makeShape = [&sshapes, &index, mesh](biterator begin, biterator end)->void {
        std::map<int, int> verts;
        TriangleMeshShape *nmesh = new TriangleMeshShape();
        nmesh->face.reserve(end - begin);
        if (!mesh->face_filename.empty())
            nmesh->face_filename = mesh->face_filename + "-" + std::to_string(index);
        std::for_each(begin, end, [&](const BuildItem &p) {
            const vec3i &face = mesh->face.at(p.index);
            int f0 = verts.insert({ face.x, (int)verts.size() }).first->second;
            int f1 = verts.insert({ face.y, (int)verts.size() }).first->second;
            int f2 = verts.insert({ face.z, (int)verts.size() }).first->second;
            nmesh->face.push_back(vec3i { f0, f1, f2 });
        });
        
        nmesh->pos.resize(verts.size());
        if (!mesh->pos_filename.empty())
            nmesh->pos_filename = mesh->pos_filename + "-" + std::to_string(index);
        
        if (!mesh->norm.empty()) {
            nmesh->norm.resize(verts.size());
            if (!mesh->norm_filename.empty())
                nmesh->norm_filename = mesh->norm_filename + "-" + std::to_string(index);
        }
        if (!mesh->uv.empty()) {
            nmesh->uv.resize(verts.size());
            if (!mesh->uv_filename.empty())
                nmesh->uv_filename = mesh->uv_filename + "-" + std::to_string(index);
        }
        std::for_each(verts.begin(), verts.end(), [&](const std::pair<int, int> &p) {
            nmesh->pos[p.second] = mesh->pos[p.first];
            if (!mesh->norm.empty()) nmesh->norm[p.second] = mesh->norm[p.first];
            if (!mesh->uv.empty()) nmesh->uv[p.second] = mesh->uv[p.first];
        });
        index++;
        save(nmesh);
        sshapes.push_back(nmesh);
    };
    
    splitShape(items.begin(), items.end(), maxtriangles, makeShape);
    unload(shape);
    return sshapes;
}

int main(int argc, char** argv) {
    if (argc <= 1) {
        error("asubdiv scene.json [trianglearea]");
        return 0;
    }
    scenefilename = argv[1];

    scene = loadScene(scenefilename, false);
    Progress ps;
    if (argc <= 2) {
        uint64_t mint, maxt, total;
        ps.start_single("Computer triangle number");
        std::tie(mint, maxt, total) = computeTriangleNumbers(scene->shapes);
        ps.end();
        message_va("Triangle Numbers: min %lu, max %lu, total %lu", mint, maxt, total);
    } else {
        uint limit;
        sscanf(argv[2], "%d", &limit);
        
        std::vector<Surface*> nsurfaces;
        std::vector<Shape*> nshapes;
        std::unordered_map<Shape*, std::vector<Surface*>> ss;
        ps.progress_start("Find shapes need spliting");
        for (int s = 0; s < scene->shapes.size(); s++) {
            Shape *shape = scene->shapes[s];
            int triNum = triangleNum(shape);
            if (triNum > limit) {
                ss.insert({ shape, {} });
            } else {
                nshapes.push_back(shape);
            }
            ps.progress_update((float)(s+1) / scene->surfaces.size());
        }
        ps.progress_end();
        
        if (ss.empty()) return 0;
        message_va("%d Shapes need to be split", ss.size());
        
        ps.progress_start("Computer association processing surfaces");
        for (int s = 0; s < scene->surfaces.size(); s++) {
            Surface* surface = scene->surfaces[s];
            auto it = ss.find(surface->shape);
            if (it != ss.end()) it->second.push_back(surface);
            else nsurfaces.push_back(surface); 
            ps.progress_update((float)(s+1) / scene->surfaces.size());
        }
        ps.progress_end();
        
        ps.progress_start("Process shapes");
        int p = 0;
        for (auto it = ss.begin(); it != ss.end(); ++it) {
            Shape *shape = it->first;
            std::vector<Surface*> &surfaces = it->second;
            auto sshapes = splitShape(shape, limit);
            nshapes.insert(nshapes.end(), sshapes.begin(), sshapes.end());
            std::for_each(surfaces.begin(), surfaces.end(), [&](Surface* surface) {
                for (int s = 0; s < sshapes.size(); s++) {
                    Surface *nsurface = new Surface(*surface);
                    nsurface->name = surface->name + "-" + std::to_string(s);
                    nsurface->shape = sshapes[s];
                    nsurfaces.push_back(nsurface);
                }
                delete surface;
            });
            remove(shape);
            delete shape;
            ps.progress_update((float)(++p) / ss.size());
        }
        ps.progress_end();
        scene->surfaces = std::move(nsurfaces);
        scene->shapes = std::move(nshapes);
        message_va("%d shapes, %d surfaces", scene->shapes.size(), scene->surfaces.size());
        SceneWriteOptions opts { SceneWriteOptions::IMAGEMODE_NONE,
            SceneWriteOptions::MESHMODE_FILENAMEONLY };
        message("Write scenefile");
        saveScene(scenefilename, scene, opts);
    }
}
