#include <common/pam.h>
#include <common/stdheader.h>
#include <common/progress.h>
#include <json.h>
#include <debug.h>
#include <scene/scene.h>
#include <scene/sceneio.h>
#include <scene/shape.h>
#include <scene/transform.h>

string scenefilename = "scene.json";
Scene *scene = 0;
int level = 2;

// LoopSubdiv Macros
inline int NEXT(int i) { return (i + 1) % 3; }
inline int PREV(int i) { return (i + 2) % 3; }

struct SDFace;
struct SDFace;
struct SDVertex {
    SDVertex(vec3f pt = zero3f) : P(pt), child(-1){ }
    vec3f P;
    vec3f N;
    vec2f uv;
    int child;
};

struct SDFace {
    // SDFace Constructor
    SDFace() {
        int i;
        for (i = 0; i < 3; ++i) v[i] = -1;
        for (i = 0; i < 4; ++i) children[i] = -1;
    }
    int v[3];
    int children[4];
};


struct SDEdge {
    // SDEdge Constructor
    SDEdge(int v0 = -1, int v1 = -1) {
        v[0] = min(v0, v1);
        v[1] = max(v0, v1);
        f[0] = f[1] = -1;
    }
    
    // SDEdge Comparison Function
    bool operator<(const SDEdge &e2) const {
        if (v[0] == e2.v[0]) return v[1] < e2.v[1];
        return v[0] < e2.v[0];
    }
    int v[2];
    int f[2];
};

void initialize(std::vector<SDVertex> &vertices, std::vector<SDFace> &faces,
                std::vector<vec3f> &pos, std::vector<vec3i> &face,
                std::vector<vec3f> &norm, std::vector<vec2f> &uv) {
    for (int i = 0; i < pos.size(); ++i) {
        vertices.emplace_back(pos[i]);
        if (norm.size()) vertices[i].N = norm[i];
        if (uv.size()) vertices[i].uv = uv[i];
    }
    
    for (int i = 0; i < face.size(); ++i) {
        faces.emplace_back();
        SDFace &f = faces[i];
        f.v[0] = face[i].x;
        f.v[1] = face[i].y;
        f.v[2] = face[i].z;
    }
}

bool refine(std::vector<SDVertex> &v, std::vector<SDFace> &f,
            std::vector<vec3f> &npos, std::vector<vec3i> &nface,
            std::vector<vec3f> &nnorm, std::vector<vec2f> &nuv, float avearea) {
    bool refined = false;
    std::vector<SDFace> nf;
    std::vector<SDVertex> nv;
    
    while (true) {
        bool done = true;
        float maxarea = 0.0f;
        for (int i = 0; i < f.size(); ++i) {
            const SDFace &face = f[i];
            const vec3f &v0 = v[face.v[0]].P;
            const vec3f &v1 = v[face.v[1]].P;
            const vec3f &v2 = v[face.v[2]].P;
            float a = triangleArea(v0, v1, v2);
            maxarea = std::max(maxarea, a);
            if (a > avearea) done = false;
        }

        if (done) break;
//        newFaces.reserve(f.size() * 4);
//        newVertices.reserve(v.size() + f.size() * 3);

        // Allocate next level of children in mesh tree
        for (uint32_t j = 0; j < v.size(); ++j) {
            nv.emplace_back();
            v[j].child = nv.size() - 1;
            nv[v[j].child].P = v[j].P;
            nv[v[j].child].N = v[j].N;
            nv[v[j].child].uv = v[j].uv;
        }
        
        for (uint32_t j = 0; j < f.size(); ++j) {
            for (int k = 0; k < 4; ++k) {
                nf.emplace_back();
                f[j].children[k] = nf.size() - 1;
            }
        }
        
        std::map<SDEdge, int> edgeVerts;
        for (uint32_t j = 0; j < f.size(); ++j) {
            SDFace *face = &f[j];
            for (int k = 0; k < 3; ++k) {
                // Compute odd vertex on _k_th edge
                SDEdge edge(face->v[k], face->v[NEXT(k)]);
                auto it = edgeVerts.find(edge);
                if (it == edgeVerts.end()) {
                    // Create and initialize new odd vertex
                    nv.emplace_back();
                    SDVertex &vert = nv.back();
                    vert.P =  v[edge.v[0]].P * 0.5f;
                    vert.P += v[edge.v[1]].P * 0.5f;
                    
                    if (nnorm.size()) {
                        vert.N = normalize(v[edge.v[0]].N * 0.5f + v[edge.v[1]].N * 0.5f);
                    }
                    
                    if (nuv.size()) {
                        vert.uv = v[edge.v[0]].uv * 0.5f + v[edge.v[1]].uv * 0.5f;
                    }
                    edgeVerts[edge] = nv.size() - 1;
                }
            }
        }
        
        // Update face vertex pointers
        for (uint32_t j = 0; j < f.size(); ++j) {
            const SDFace &face = f[j];
            for (int k = 0; k < 3; ++k) {
                // Update child vertex pointer to new even vertex
                nf[face.children[k]].v[k] = v[face.v[k]].child;
                
                // Update child vertex pointer to new odd vertex
                int vi = edgeVerts[SDEdge(face.v[k], face.v[NEXT(k)])];
                nf[face.children[k]].v[NEXT(k)] = vi;
                nf[face.children[NEXT(k)]].v[k] = vi;
                nf[face.children[3]].v[k] = vi;
            }
        }
        // Prepare for next level of subdivision
        nf.swap(f); nf.clear();
        nv.swap(v); nv.clear();
        refined = true;
    }
    
    npos.resize(v.size());
    if (nnorm.size()) nnorm.resize(v.size());
    if (nuv.size()) nuv.resize(v.size());
    
    for (unsigned int i = 0; i < v.size(); i++) {
        const SDVertex &sdv = v[i];
        npos[i] = sdv.P;
        if (nnorm.size()) nnorm[i] = sdv.N;
        if (nuv.size()) nuv[i] = sdv.uv;
    }
    
    // Create _TriangleMesh_ from subdivision mesh
    uint32_t ntris = uint32_t(f.size());
    nface.resize(ntris);
    vec3i *vp = &nface[0];
//    uint32_t totVerts = uint32_t(v.size());
//    std::map<SDVertex*, int> usedVerts;
//    for (uint32_t i = 0; i < totVerts; ++i)
//        usedVerts[&v[i]] = i;
//    for (uint32_t i = 0; i < ntris; ++i) {
//        vp->x = usedVerts[f[i].v[0]];
//        vp->y = usedVerts[f[i].v[1]];
//        vp->z = usedVerts[f[i].v[2]];
//        ++vp;
//    }
    for (uint32_t i = 0; i < ntris; ++i) {
        vp->x = f[i].v[0];
        vp->y = f[i].v[1];
        vp->z = f[i].v[2];
        ++vp;
    }
    return refined;
}

bool subdiv(std::vector<vec3f> &pos, std::vector<vec3i> &face, std::vector<vec3f> &norm, std::vector<vec2f> &uv, float avearea) {
    std::vector<SDVertex> vertices;
    std::vector<SDFace> faces;
    initialize(vertices, faces, pos, face, norm, uv);
    return refine(vertices, faces, pos, face, norm, uv, avearea);
}

bool subdiv(Shape* shape, float area) {
    if (shape->type != TriangleMeshShape::TYPEID) return false;
    load(shape);
    TriangleMeshShape* mesh = (TriangleMeshShape*)shape;
    bool rt = subdiv(mesh->pos, mesh->face, mesh->norm, mesh->uv, area);
    save(shape);
    unload(shape);
    return rt;
}


range3f computeWorldBoundingBox(const std::vector<Surface*> &surfaces, range1f &areaRange, float &aveArea) {
    Progress ps;
    areaRange = invalidrange1f;
    range3f worldBound = invalidrange3f;
    float areaSum = 0.0f;
    int nTriangle = 0;
    ps.progress_start("Compute triangle area and world bound");
    for (int s = 0; s < surfaces.size(); s++) {
        const Surface *surface = surfaces[s];
        range3f bbox = invalidrange3f;
        load(surface->shape);
        switch (surface->shape->type) {
            case TriangleMeshShape::TYPEID:
            case HairMeshShape::TYPEID: {
                const TriangleMeshShape* mesh = (const TriangleMeshShape*)surface->shape;
                for(int i = 0; i < mesh->face.size(); i ++) {
                    const vec3i& f = mesh->face[i];
                    const vec3f v0 = transformPoint(surface->transform, mesh->pos[f.x]);
                    const vec3f v1 = transformPoint(surface->transform, mesh->pos[f.y]);
                    const vec3f v2 = transformPoint(surface->transform, mesh->pos[f.z]);
                    bbox.grow(triangleBoundingBox(v0, v1, v2));
                    float area = triangleArea(v0, v1, v2);
                    areaRange.grow(area);
                    areaSum += area;
                    nTriangle++;
                }
            } break;
            case QuadShape::TYPEID: {
                const QuadShape* shape = (const QuadShape*)(surface->shape);
                float w = shape->width / 2.0f;
                float h = shape->height / 2.0f;
                bbox.grow(transformPoint(surface->transform, vec3f(-w, -h, 0)));
                bbox.grow(transformPoint(surface->transform, vec3f(-w,  h, 0)));
                bbox.grow(transformPoint(surface->transform, vec3f( w,  h, 0)));
                bbox.grow(transformPoint(surface->transform, vec3f( w, -h, 0)));
            } break;
            case SphereShape::TYPEID: {
                const SphereShape* shape = (const SphereShape*)(surface->shape);
                float r = shape->radius;
                vec3f center = transformPoint(surface->transform, vec3f(0, 0, 0));
                bbox = range3f(center + vec3f(-r, -r, -r), center + vec3f(r, r, r));
            } break;
            default: error("unknown shape type"); break;
        }
        worldBound.grow(bbox);
        unload(surface->shape);
        ps.progress_update((float)(s+1) / surfaces.size());
    }
    ps.progress_end();
    aveArea = areaSum / nTriangle;
    return worldBound;
}

int main(int argc, char** argv) {
    if (argc <= 1) {
        error("asubdiv scene.json [trianglearea]");
        return 0;
    }
    scenefilename = argv[1];
    scene = loadScene(scenefilename, false);
    message("Scene loaded");
    
    if (argc <= 2) {
        range1f areaRange = invalidrange1f;
        float aveArea = 0.0f;
        range3f worldBound = computeWorldBoundingBox(scene->surfaces, areaRange, aveArea);
        message_va("bbox: %s, area-range %s, average-area: %f",
                   worldBound.extent().print().c_str(), areaRange.print().c_str(), aveArea);
    } else {
        float area;
        sscanf(argv[2], "%f", &area);
        bool refined = false;
        Progress ps;
        ps.progress_start("Subdivide surfaces");
        for (int s = 0; s < scene->shapes.size(); s++) {
            bool ret = subdiv(scene->shapes[s], area);
            if (ret) { refined = ret; }
            ps.progress_update((float)(s+1) / scene->shapes.size());
        }
        ps.progress_end();
        if (refined) message("Scene refined\n");
    }
 }
