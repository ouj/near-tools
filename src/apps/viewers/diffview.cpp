#include <common/pam.h>
#include <vector>
using std::vector;
#include <cstring>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// exrview
// view and convert exr images
// usage:
// exrview -g gamma -e exposure -a im.exr ...
// where
// im.exr ... is a list of exr images
// g is an optional gamma (1 default)
// e is an optional exposure (1 default)
// -a automatically compute gamma/exposure to fit the current view
//
// hit h in the window to get info on keyboard commands

vec3f HsvToRgb(float h, float s, float v) {
    if (s == 0) {
        return one3f;
    }

    h /= 60;
    int i  = (int)h;
    float f = h - i;
    float p = v *  (1 - s);
    float q = v * (1 - s * f);
    float t = v * (1 - s * (1 - f));

    float r, g, b;
    switch( i ) {
        case 0:
            r = v;
            g = t;
            b = p;
            break;
        case 1:
            r = q;
            g = v;
            b = p;
            break;
        case 2:
            r = p;
            g = v;
            b = t;
            break;
        case 3:
            r = p;
            g = q;
            b = v;
            break;
        case 4:
            r = t;
            g = p;
            b = v;
            break;
        default:        // case 5:
            r = v;
            g = p;
            b = q;
            break;
    }
    return vec3f(r, g, b);
}


inline vec2i operator*(const vec2i& v, float s) {
    vec2i ret;
    ret.x = int(v.x * s);
    ret.y = int(v.y * s);
    return ret;
}

enum ErrorMode { L2_ERROR, L1_ERROR, PERCENTAGE_ERROR };
int errMode = L2_ERROR;

static std::string printErrorMode(const int errMode) {
    if (errMode == L2_ERROR) {
        return "L2 Error";
    } else if (errMode == PERCENTAGE_ERROR) {
        return "Percentage Error";
    } else if (errMode == L1_ERROR) {
        return "L1 Error";
    } else error("unknown error mode");
    return std::string();
}

GLuint textureId = 0;
bool updateTextureRequired = true;

sarray2<vec3f> correctedImage;
sarray2<float> diffImage;
sarray2<vec3f> image1, image2;

string currentName;
vec2i viewInspectionLocation = vec2i(0,0);
float currentImageMax = 0;

vec2i imageSize;
vec2i viewSize;

void* HUD_FONT = GLUT_BITMAP_9_BY_15;
int HUD_LINEHEIGHT = 10;
int HUD_MARGIN = 8;

float exposureVal = 0;
float logVal = 1;

int zoomLevel = 0;

int infoMode = 0;
#define MAX_INFOMODE 3

int hudMode = 2;
#define MAX_HUDMODE 3
int getHudHeight() {
    switch(hudMode) {
        case 1: return HUD_LINEHEIGHT + 2*HUD_MARGIN;
        case 2: return 2*HUD_LINEHEIGHT + 3*HUD_MARGIN;
    }
    return 0;
}

vec2i rasterOrigin;

inline int __pow2(int n) { return 1 << n; }

float rasterScaleFactor() { return powf(2, zoomLevel); }
bool isRasterMinified() { return zoomLevel < 0; }
int scaleRatio() { return __pow2(abs(zoomLevel)); }

vec2i imageToView(vec2i v) { return rasterOrigin + v * rasterScaleFactor(); }
vec2i viewToImage(vec2i v) { return (v - rasterOrigin) * (1.0f / rasterScaleFactor()); }
vec2i windowToView(vec2i v) { return v - vec2i(0,getHudHeight()); }
vec2i viewToWindow(vec2i v) { return v + vec2i(0,getHudHeight()); }

bool isImageSmaller() { return !(imageSize.x > viewSize.x || imageSize.x > viewSize.y); }
bool isRasterSmaller() { return !((imageSize * rasterScaleFactor()).x > viewSize.x || (imageSize * rasterScaleFactor()).x > viewSize.y); }
void center() { rasterOrigin = (viewSize - (imageSize * rasterScaleFactor())) * 0.5f; }
void zoomOne() { zoomLevel = 0; if(isRasterSmaller()) center(); }
void zoomUp() { zoomLevel ++; if(isRasterSmaller()) center(); }
void zoomDown() { zoomLevel --; if(isRasterSmaller()) center(); }
void zoomFit() {
    if(!isImageSmaller()) {
        // minify
        zoomLevel = 0;
        while(!isRasterSmaller()) {
            zoomLevel --;
        }
    } else {
        zoomLevel = 0;
    }
    center();
}

char* getPixelInfoString(bool includeProgramName) {
    static char message[1024];
    static char fnum[64];
    static char f[10];
    vec2i imPos = viewToImage(viewInspectionLocation);
    if(imPos.x < 0 || imPos.x >= imageSize.x || imPos.y < 0 || imPos.y >= imageSize.y) {
        sprintf(message, "%s[%4d,%4d] -> out of image",
            (includeProgramName)?"exrview | ":"",
            imPos.x, imPos.y);
    } else {
        fnum[0] = '\0';
        float val = diffImage.at(imPos.x,imPos.y);
        if (val != 0.0f && (abs(val) < 0.0001 || abs(val) >= 1000))
            sprintf(f, " %#9.2e", val);
        else
            sprintf(f, " %9.4f", val);
        strcat(fnum, f);
        sprintf(message, "%s[%4d,%4d] ->%s",
            (includeProgramName)?"diffview | ":"",
            imPos.x, imPos.y, fnum);
    }
    return message;
}

char* getImageInfoString(bool includeProgramName) {
    static char message[1024];
    if(isRasterMinified()) {
        sprintf(message, "%s(%d,%d) @ 1/%dx | e -> %6.3f error x %.1f | log -> %4.2f |",
            (includeProgramName)?"exrview | ":"",
            imageSize.x, imageSize.y, scaleRatio(), exposureVal, pow(2, exposureVal), logVal);
    } else {
        sprintf(message, "%s(%d,%d) @ %dx | e -> %6.3f error x %.1f | log -> %4.2f |",
            (includeProgramName)?"exrview | ":"",
            imageSize.x, imageSize.y, scaleRatio(), exposureVal, pow(2, exposureVal), logVal);
    }
    return message;
}

char* getFileInfoString(bool includeProgramName) {
    static char message[1024];
    sprintf(message, "%s%s",
        (includeProgramName)?"diffview | ":"",
        currentName.c_str());
    return message;
}

void setTitle() {
    if(infoMode == 0) {
        glutSetWindowTitle(getFileInfoString(true));
    } else if(infoMode == 1) {
        glutSetWindowTitle(getImageInfoString(true));
    } else if(infoMode == 2) {
        glutSetWindowTitle(getPixelInfoString(true));
    }
}

inline float clamp01(float x) {
    if(x > 1) return 1;
    if(x < 0) return 0;
    return x;
}

//void exposureGamma(sarray2<vec4f>* dest, sarray2<vec4f>* src, float exposureVal, float gammaVal) {
//    int size = dest->size();
//    vec4f* d = &dest->at(0);
//    vec4f* s = &src->at(0);
//    float scale = pow(2, exposureVal);
//    float power = 1 / gammaVal;
//    for(int i = 0; i < size; i ++) {
//        d[i].x = clamp01(pow(scale*s[i].x,power));
//        d[i].y = clamp01(pow(scale*s[i].y,power));
//        d[i].z = clamp01(pow(scale*s[i].z,power));
//        d[i].w = s[i].w;
//    }
//}

sarray2<float> computeDifferenceImage(const sarray2<vec3f> &image1, const sarray2<vec3f> &image2) {
    error_if(image1.width() != image2.width() || image1.height() != image2.height(),
             "image dimension do not match");
    sarray2<float> diff(image1.width(), image1.height());

    if (errMode == L1_ERROR) {
        for(int i = 0; i < diff.size(); i ++) {
            const vec3f &i0 = image1.at(i);
            const vec3f &i1 = image2.at(i);
            vec3f d = abs_component(i0 - i1);
            diff.at(i) = d.average();
        }
    } else if (errMode == L2_ERROR) {
        for(int i = 0; i < diff.size(); i ++) {
            const vec3f &i0 = image1.at(i);
            const vec3f &i1 = image2.at(i);
            vec3f d = abs_component(i0 - i1);
            diff.at(i) = d.length();
        }
    } else if (errMode == PERCENTAGE_ERROR) {
        for(int i = 0; i < diff.size(); i ++) {
            const vec3f &i0 = image1.at(i);
            const vec3f &i1 = image2.at(i);
            vec3f d = abs_component(i0 - i1);
            d /= i0;
            diff.at(i) = d.average();
        }
    }
    return diff;
}


void computeCorrectedImage() {
    diffImage = computeDifferenceImage(image1, image2);
    currentName = printErrorMode(errMode);
    correctedImage.resize(diffImage.width(), diffImage.height());
    float scale = pow(2, -exposureVal);
    float power = 1 / logVal;
    for (uint32_t i = 0; i < correctedImage.size(); i++) {
        float va = diffImage.at(i);
        float l = va / scale;
        l = pow(l, power);
        l = max(0.0f, min(1.0f, l));
        float h = (1 - l) * 240.0;
        vec3f c = HsvToRgb(h, 1, 1);
        correctedImage.at(i)= vec3f(c.x, c.y, c.z);
    }
    updateTextureRequired = true;
}

float computeImageMax() {
    float m = 0;
    for(int i = 0; i < diffImage.size(); i ++) {
        float v = diffImage.at(i);
        m = max(v, m);
    }
    return m;
}

float computeAverageLogLuminance() {
    float avgLog = 0; int count = 0;
    for(int i = 0; i < diffImage.size(); i ++) {
        float l = diffImage.at(i);
        avgLog += log(l);
        count ++;
    }
    avgLog /= count;
    return exp(avgLog);
}

float computeAverageLuminance() {
    float avg = 0; int count = 0;
    for(int i = 0; i < diffImage.size(); i ++) {
        float l = diffImage.at(i);
        avg += l;
        count ++;
    }
    avg /= count;
    return avg;
}

void setExposureGamma11() {
    exposureVal = 0;
    logVal = 1;
}

void setExposureGamma12() {
    exposureVal = 0;
    logVal = 2.2f;
}

void computeAutoParams(bool useLog) {
    exposureVal = 0.0f; // middle gray is 0.18
    logVal = 1.0f;
}


void loadImages(const string &filename1, const string &filename2) {
    image1 = loadPnm3f(filename1);
    image2 = loadPnm3f(filename2);

    currentName = printErrorMode(errMode);
    imageSize = vec2i(image1.width(), image1.height());
    currentImageMax = computeImageMax();
    computeCorrectedImage();

    zoomOne();
}

void drawQuad(vec2i origin, vec2i corner) {
    glBegin(GL_QUADS);
        glTexCoord2i(0,0);
        glVertex2i(origin.x,origin.y);
        glTexCoord2i(1,0);
        glVertex2i(corner.x,origin.y);
        glTexCoord2i(1,1);
        glVertex2i(corner.x,corner.y);
        glTexCoord2i(0,1);
        glVertex2i(origin.x,corner.y);
    glEnd();
}

void drawString(void* font, const char* msg) {
    while(*msg != 0) {
        glutBitmapCharacter(font, *msg);
        msg ++;
    }
}

int getStringLength(void* font, const char* msg) {
    int l = 0;
    while(*msg != 0) {
        l += glutBitmapWidth(font, *msg);
        msg ++;
    }
    return l;
}

void drawView() {
    vec2i rasterOrigin = imageToView(vec2i(0,0));
    vec2i rasterCorner = imageToView(imageSize);

    glViewport(0,0,viewSize.x,viewSize.y);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,viewSize.x, viewSize.y,0,-1,1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);
    if(updateTextureRequired) {
        if(textureId == 0) {
            glGenTextures(1, &textureId);
            glBindTexture(GL_TEXTURE_2D, textureId);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                imageSize.x, imageSize.y,
                0, GL_RGB, GL_FLOAT, &correctedImage.at(0));
        } else {
            glTexSubImage2D(GL_TEXTURE_2D, 0,
                0, 0, imageSize.x, imageSize.y,
                GL_RGB, GL_FLOAT, &correctedImage.at(0));
        }
        updateTextureRequired = false;
    }

    glColor4f(1,1,1,1);
    drawQuad(rasterOrigin, rasterCorner);

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void drawHud() {
    glViewport(0,viewSize.y,viewSize.x,getHudHeight());
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,viewSize.x, 0, getHudHeight(),-1,1);

    glColor4f(0,0,0,1);
    drawQuad(vec2i(0,0), vec2i(viewSize.x,getHudHeight()));

    char* pixelInfo = getPixelInfoString(false);
    char* imageInfo = getImageInfoString(false);
    glColor4f(1,1,1,1);
    if(hudMode == 1) {
        if(getStringLength(HUD_FONT, imageInfo) < viewSize.x) {
            glRasterPos2i(viewSize.x/2-getStringLength(HUD_FONT, imageInfo)/2,HUD_MARGIN);
        } else {
            glRasterPos2i(HUD_MARGIN,HUD_MARGIN);
        }
        drawString(HUD_FONT, imageInfo);
    } else if(hudMode == 2) {
        if(getStringLength(HUD_FONT, imageInfo) < viewSize.x && getStringLength(HUD_FONT, pixelInfo) < viewSize.x) {
            glRasterPos2i(viewSize.x/2-getStringLength(HUD_FONT, pixelInfo)/2,HUD_MARGIN);
        } else {
            glRasterPos2i(HUD_MARGIN,HUD_MARGIN);
        }
        drawString(HUD_FONT, pixelInfo);
        if(getStringLength(HUD_FONT, imageInfo) < viewSize.x && getStringLength(HUD_FONT, pixelInfo) < viewSize.x) {
            glRasterPos2i(viewSize.x/2-getStringLength(HUD_FONT, imageInfo)/2,2*HUD_MARGIN+HUD_LINEHEIGHT);
        } else {
            glRasterPos2i(HUD_MARGIN,2*HUD_MARGIN+HUD_LINEHEIGHT);
        }
        drawString(HUD_FONT, imageInfo);
    }
}

void display() {
    glClearColor(0.5,0.5,0.5,0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawView();
    if(hudMode != 0) drawHud();

    glutSwapBuffers();
}

void printHelp() {
    printf("------------ mouse\n");
    printf("left -> move image\n");
    printf("\n");

    printf("------------ keyboard\n");
    printf("  [   -> increase exposure\n");
    printf("  ]   -> decrease exposure\n");
    printf("  e   -> input exposure\n");
    printf("  ;   -> increase gamma\n");
    printf("  \'   -> decrease gamma\n");
    printf("  g   -> input gamma\n");
    printf("  a   -> auto exposure with avg luminance\n");
    printf("  A   -> auto exposure with avg log luminance\n");
    printf("  1   -> set exposure to 1 and gamma to 1\n");
    printf("  2   -> set exposure to 1 and gamma to 2.2\n");
    printf("  0   -> set zoom to 1x\n");
    printf("  -   -> descrease zoom\n");
    printf("  =   -> increase zoom\n");
    printf("  f   -> zoom down to fit\n");
    printf("  i   -> switch title info mode\n");
    printf("  h   -> switch hud info mode\n");
    printf("  s   -> save image difference\n");
    printf("  S   -> save scale image\n");
    printf("  ?   -> print this message\n");
    printf("\n");
}

void reshape(int w, int h) {
    viewSize.x = w;
    viewSize.y = h - getHudHeight();

    if(isRasterSmaller()) center();

    glutPostRedisplay();
}

float getTypeValue(const char* message) {
    float ret;
    printf("%s", message);
    warning_if_not(scanf("%f", &ret) == 1, "no input value");
    return ret;
}

void saveScaleImage()
{
    const int width = 1000;
    const int height = 100;
    sarray2<vec3f> scaleImg(width, height);
    for (int i = 0; i < width; i++) {
        float h = (float)i / width;
        vec3f c = HsvToRgb((1-h)*240, 1, 1);
        for (int j = 0; j < height; j++) {
            if(abs(i - 250) < 4 || abs(i - 500) < 4 || abs(i - 750) < 4) {
                if (j >= 50) {
                    scaleImg.at(i, j) = zero3f;
                    continue;
                }
            }

            scaleImg.at(i, j) = c;
        }
    }
    string saveName = "scale.pfm";
    savePfm(saveName, scaleImg);
    float scale = pow(2, exposureVal);
    message_va("%f %f %f %f %f",
               pow(0.0f*scale, logVal),
               pow(0.25f*scale, logVal),
               pow(0.5f*scale, logVal),
               pow(0.75f*scale, logVal),
               pow(1.0f*scale, logVal));
}

void keyboard(unsigned char c, int x, int y) {
    switch(c) {
        case '[':
            exposureVal -= 1;
            computeCorrectedImage();
            break;
        case ']':
            exposureVal += 1;
            computeCorrectedImage();
            break;
        case 'e':
            exposureVal = getTypeValue("input exposure value: ");
            computeCorrectedImage();
            break;
        case ';':
            logVal -= 0.1f; if(logVal < 0.1f) logVal = 0.1f;
            computeCorrectedImage();
            break;
        case '\'':
            logVal += 0.1f;
            computeCorrectedImage();
            break;
        case 'g':
            logVal = getTypeValue("input log value: ");
            computeCorrectedImage();
            break;
        case 'a':
            computeAutoParams(true);
            computeCorrectedImage();
            break;
        case 'A':
            computeAutoParams(false);
            computeCorrectedImage();
            break;
        case '1':
            setExposureGamma11();
            computeCorrectedImage();
            break;
        case '2':
            setExposureGamma12();
            computeCorrectedImage();
            break;
        case '0':
            zoomOne();
            break;
        case '-':
            zoomDown();
            break;
        case '+':
        case '=':
            zoomUp();
            break;
        case 'f':
            zoomFit();
            break;
        case 'i':
            infoMode = (infoMode + 1) % MAX_INFOMODE;
            break;
        case 'h':
            hudMode = (hudMode + 1) % MAX_HUDMODE;
            reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
            break;
        case 'm':
            errMode = (errMode + 1) % 3;
            computeCorrectedImage();
            break;
        case 's':
            savePfm("diffImage.pfm", correctedImage);
            break;
        case 'S':
            saveScaleImage();
            break;
        case '?':
            printHelp();
            break;
    }
    setTitle();
    glutPostRedisplay();
}


bool leftMouseDown = false;
bool rightMouseDown = false;
vec2i mouseBegin;
vec2i rasterOriginBegin;
bool moveRaster = false;
void mouse(int button, int state, int x, int y) {
    leftMouseDown = button == GLUT_LEFT_BUTTON && state == GLUT_DOWN;
    rightMouseDown = button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN;
    mouseBegin = vec2i(x,y);

    moveRaster = leftMouseDown && !isRasterSmaller();

    if(moveRaster) {
        rasterOriginBegin = rasterOrigin;
    }

    viewInspectionLocation = windowToView(vec2i(x,y));
    setTitle();
    glutPostRedisplay();
}

void motion(int x, int y) {
    vec2i delta = vec2i(x,y) - mouseBegin;

    if(moveRaster) {
        rasterOrigin = rasterOriginBegin + delta;
    }

    viewInspectionLocation = windowToView(vec2i(x,y));
    setTitle();
    glutPostRedisplay();
}

void passivemotion(int x, int y) {
    viewInspectionLocation = windowToView(vec2i(x,y));
    setTitle();
    glutPostRedisplay();
}

void initGui(int argc, char** argv) {
	glutInit(&argc, argv);

    viewSize = imageSize;
    vec2i windowSize = vec2i(imageSize.x,viewSize.y+getHudHeight());

	glutInitDisplayMode(GLUT_RGB | GLUT_ALPHA | GLUT_DOUBLE);
	glutInitWindowPosition(0,0);
    glutInitWindowSize(windowSize.x, windowSize.y);
    glutCreateWindow("diffview");
	setTitle();
    glutSetCursor( GLUT_CURSOR_CROSSHAIR );

	// callbacks
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutPassiveMotionFunc(passivemotion);
    glutReshapeFunc(reshape);
}

void runGui() {
    glutMainLoop();
}

int main(int argc, char** argv) {
    const char* usage = "diffview image1.pfm image2.pfm";
    if (argc < 3) {
        error(usage);
        return 0;
    }
    string filename1 = argv[1];
    string filename2 = argv[2];

    loadImages(filename1, filename2);
    computeCorrectedImage();

#ifdef  WIN32
    _set_output_format(_TWO_DIGIT_EXPONENT);
#endif

    initGui(argc, argv);
    runGui();
}
