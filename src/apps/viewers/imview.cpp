#include <common/pam.h>
#include <vector>
using std::vector;
#include <cstring>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// exrview
// view and convert exr images
// usage:
// exrview -g gamma -e exposure -a im.exr ...
// where
// im.exr ... is a list of exr images
// g is an optional gamma (1 default)
// e is an optional exposure (1 default)
// -a automatically compute gamma/exposure to fit the current view
//
// hit h in the window to get info on keyboard commands

inline vec2i operator*(const vec2i& v, float s) {
    vec2i ret;
    ret.x = int(v.x * s);
    ret.y = int(v.y * s);
    return ret;
}

template <typename R>
struct vec4 {
    R x, y, z, w;
    vec4() {}
    vec4(R x, R y, R z, R w) : x(x), y(y), z(z), w(w) {}
};

typedef vec4<float> vec4f;
typedef vec4<double> vec4d;
typedef vec4<int> vec4i;

GLuint textureId = 0;
bool updateTextureRequired = true;

vector<sarray2<vec4f>*> images;
vector<string> filenames;

int currentImageIdx = 0;
sarray2<vec4f>* currentImage = 0;
string currentName = "";
sarray2<vec4f>* correctedImage = 0;
vec2i viewInspectionLocation = vec2i(0,0);
float currentImageMax = 0;

vec2i imageSize;
vec2i viewSize;

void* HUD_FONT = GLUT_BITMAP_9_BY_15;
int HUD_LINEHEIGHT = 10;
int HUD_MARGIN = 8;

float gammaVal = 2.2;
float exposureVal = 0;

int zoomLevel = 0;

int infoMode = 0;
#define MAX_INFOMODE 3

int hudMode = 2;
#define MAX_HUDMODE 3
int getHudHeight() {
    switch(hudMode) {
        case 1: return HUD_LINEHEIGHT + 2*HUD_MARGIN;
        case 2: return 2*HUD_LINEHEIGHT + 3*HUD_MARGIN;
    }
    return 0;
}

vec2i rasterOrigin;

inline int __pow2(int n) { return 1 << n; }

float rasterScaleFactor() { return powf(2, zoomLevel); }
bool isRasterMinified() { return zoomLevel < 0; }
int scaleRatio() { return __pow2(abs(zoomLevel)); }

vec2i imageToView(vec2i v) { return rasterOrigin + v * rasterScaleFactor(); }
vec2i viewToImage(vec2i v) { return (v - rasterOrigin) * (1.0f / rasterScaleFactor()); }
vec2i windowToView(vec2i v) { return v - vec2i(0,getHudHeight()); }
vec2i viewToWindow(vec2i v) { return v + vec2i(0,getHudHeight()); }

bool isImageSmaller() { return !(imageSize.x > viewSize.x || imageSize.x > viewSize.y); }
bool isRasterSmaller() { return !((imageSize * rasterScaleFactor()).x > viewSize.x || (imageSize * rasterScaleFactor()).x > viewSize.y); }
void center() { rasterOrigin = (viewSize - (imageSize * rasterScaleFactor())) * 0.5f; }
void zoomOne() { zoomLevel = 0; if(isRasterSmaller()) center(); }
void zoomUp() { zoomLevel ++; if(isRasterSmaller()) center(); }
void zoomDown() { zoomLevel --; if(isRasterSmaller()) center(); }
void zoomFit() {
    if(!isImageSmaller()) {
        // minify
        zoomLevel = 0;
        while(!isRasterSmaller()) {
            zoomLevel --;
        }
    } else {
        zoomLevel = 0;
    }
    center();
}

char* getPixelInfoString(bool includeProgramName) {
    static char message[1024];
    static char fnum[64];
    static char f[10];
    vec2i imPos = viewToImage(viewInspectionLocation);
    if(imPos.x < 0 || imPos.x >= imageSize.x || imPos.y < 0 || imPos.y >= imageSize.y) {
        sprintf(message, "%s[%4d,%4d] -> out of image",
            (includeProgramName)?"exrview | ":"",
            imPos.x, imPos.y);
    } else {
        fnum[0] = '\0';
        vec4f val4f = currentImage->at(imPos.x,imPos.y);
        float* val = &val4f.x;
        for (int i = 0; i < 4; i++) {
            if (val[i] != 0.0f && (abs(val[i]) < 0.0001 || abs(val[i]) >= 1000))
                sprintf(f, " %#9.2e", val[i]);
            else
                sprintf(f, " %9.4f", val[i]);
            strcat(fnum, f);
        }
        sprintf(message, "%s[%4d,%4d] ->%s",
            (includeProgramName)?"exrview | ":"",
            imPos.x, imPos.y, fnum);
    }
    return message;
}

char* getImageInfoString(bool includeProgramName) {
    static char message[1024];
    if(isRasterMinified()) {
        sprintf(message, "%s(%d,%d) @ 1/%dx | e -> %6.3f g -> %4.2f | m -> %9.4f",
            (includeProgramName)?"exrview | ":"",
            imageSize.x, imageSize.y, scaleRatio(), exposureVal, gammaVal, currentImageMax);
    } else {
        sprintf(message, "%s(%d,%d) @ %dx | e -> %6.3f g -> %4.2f | m -> %9.4f",
            (includeProgramName)?"exrview | ":"",
            imageSize.x, imageSize.y, scaleRatio(), exposureVal, gammaVal, currentImageMax);
    }
    return message;
}

char* getFileInfoString(bool includeProgramName) {
    static char message[1024];
    sprintf(message, "%s%s",
        (includeProgramName)?"exrview | ":"",
        currentName.c_str());
    return message;
}

void setTitle() {
    if(infoMode == 0) {
        glutSetWindowTitle(getFileInfoString(true));
    } else if(infoMode == 1) {
        glutSetWindowTitle(getImageInfoString(true));
    } else if(infoMode == 2) {
        glutSetWindowTitle(getPixelInfoString(true));
    }
}

inline float clamp01(float x) {
    if(x > 1) return 1;
    if(x < 0) return 0;
    return x;
}

void exposureGamma(sarray2<vec4f>* dest, sarray2<vec4f>* src, float exposureVal, float gammaVal) {
    int size = dest->size();
    vec4f* d = &dest->at(0);
    vec4f* s = &src->at(0);
    float scale = pow(2, exposureVal);
    float power = 1 / gammaVal;
    for(int i = 0; i < size; i ++) {
        d[i].x = clamp01(pow(scale*s[i].x,power));
        d[i].y = clamp01(pow(scale*s[i].y,power));
        d[i].z = clamp01(pow(scale*s[i].z,power));
        d[i].w = s[i].w;
    }
}

void computeCorrectedImage() {
    if(!correctedImage) correctedImage = new sarray2<vec4f>(currentImage->width(), currentImage->height());
    exposureGamma(correctedImage, currentImage, exposureVal, gammaVal);
    updateTextureRequired = true;
}

float computeImageMax() {
    float m = 0;
    for(int i = 0; i < currentImage->size(); i ++) {
        vec4f v = currentImage->at(i);
        m = max(v.x, max(v.y, max(v.z, m)));
    }
    return m;
}

float computeAverageLogLuminance() {
    float avgLog = 0; int count = 0;
    for(int i = 0; i < currentImage->size(); i ++) {
        vec4f v = currentImage->at(i);
        if(v.w <= 0.001) continue;
        float l = (v.x+v.y+v.z)/3 + 1e-4f;
        avgLog += log(l);
        count ++;
    }
    avgLog /= count;
    return exp(avgLog);
}

float computeAverageLuminance() {
    float avg = 0; int count = 0;
    for(int i = 0; i < currentImage->size(); i ++) {
        vec4f v = currentImage->at(i);
        if(v.w <= 0.001) continue;
        float l = (v.x+v.y+v.z)/3;
        avg += l;
        count ++;
    }
    avg /= count;
    return avg;
}

void setCurrentImage(int ci) {
    currentImageIdx = ci;
    currentImage = images[currentImageIdx];
    currentName = filenames[currentImageIdx];
    imageSize = vec2i(currentImage->width(), currentImage->height());
    currentImageMax = computeImageMax();
    computeCorrectedImage();
}

void setExposureGamma11() {
    exposureVal = 0;
    gammaVal = 1;
}

void setExposureGamma12() {
    exposureVal = 0;
    gammaVal = 2.2f;
}

void computeAutoParams(bool useLog) {
    /*
    float m = computeImageMax();
    exposureVal = log(1.0f / m) / log(2.0f);
    */
    float avg = 0;
    if(useLog) { avg = computeAverageLogLuminance(); }
    else { avg = computeAverageLuminance(); }
    exposureVal = log(0.18f / avg) / log(2.0f); // middle gray is 0.18
    gammaVal = 2.2f;
}

void computeDifferenceImage() {
    if(images.size() != 2) {
        printf("Only computes difference when exactly two images are loaded\n");
        return;
    }

    vec3f L1 = zero3f;
    vec3f L2 = zero3f;
    float max_error = 0.0;
    sarray2<vec4f>* difference = new sarray2<vec4f>(images[0]->width(), images[0]->height());
    for(int i = 0; i < difference->size(); i ++) {
        vec4f i0 = images[0]->at(i); vec4f i1 = images[1]->at(i);
        vec3f v0 = max_component(zero3f, vec3f(i0.x, i0.y, i0.z));
        vec3f v1 = max_component(zero3f, vec3f(i1.x, i1.y, i1.z));
        vec3f d = v0 - v1;
        L1 += abs_component(d);
        L2 += d * d;
        max_error = max(max_error, abs_component(d).average());
        difference->at(i).x = fabs(i0.x-i1.x);
        difference->at(i).y = fabs(i0.y-i1.y);
        difference->at(i).z = fabs(i0.z-i1.z);
        difference->at(i).w = max(i0.w,i1.w);
    }
    L2 = sqrt_component(L2);
    images.push_back(difference);
    filenames.push_back("difference.pfm");
    message_va("L1 error: %f", L1.average());
    message_va("L2 error: %f", L2.average());
    message_va("max pixel error: %f", max_error);
}

void loadImages(const vector<string>& inputFilenames) {
    for(size_t i = 0; i < inputFilenames.size(); i ++) {
        string filename = inputFilenames[i];
        sarray2<vec3f> image = loadPnm3f(filename);
        sarray2<vec4f>* loadedImage = new sarray2<vec4f>(image.width(),image.height());
        for(int j = 0; j < image.size(); j ++) {
            vec3f v3f = image.at(j);
            vec4f v4f = vec4f(v3f.x,v3f.y,v3f.z,1);
            loadedImage->at(j) = v4f;
        }
        if(i != 0) {
            if(images[0]->width() != loadedImage->width() ||
                images[0]->height() != loadedImage->height()) {
                printf("sarray2 sizes do not match. Skipping image %s\n",
                        filename.c_str());
                continue;
            }
        }
        filenames.push_back(filename);
        images.push_back(loadedImage);
    }
    setCurrentImage(0);
    zoomOne();
}

void drawQuad(vec2i origin, vec2i corner) {
    glBegin(GL_QUADS);
        glTexCoord2i(0,0);
        glVertex2i(origin.x,origin.y);
        glTexCoord2i(1,0);
        glVertex2i(corner.x,origin.y);
        glTexCoord2i(1,1);
        glVertex2i(corner.x,corner.y);
        glTexCoord2i(0,1);
        glVertex2i(origin.x,corner.y);
    glEnd();
}

void drawString(void* font, const char* msg) {
    while(*msg != 0) {
        glutBitmapCharacter(font, *msg);
        msg ++;
    }
}

int getStringLength(void* font, const char* msg) {
    int l = 0;
    while(*msg != 0) {
        l += glutBitmapWidth(font, *msg);
        msg ++;
    }
    return l;
}

void drawView() {
    vec2i rasterOrigin = imageToView(vec2i(0,0));
    vec2i rasterCorner = imageToView(imageSize);

    glViewport(0,0,viewSize.x,viewSize.y);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,viewSize.x, viewSize.y,0,-1,1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);
    if(updateTextureRequired) {
        if(textureId == 0) {
            glGenTextures(1, &textureId);
            glBindTexture(GL_TEXTURE_2D, textureId);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                imageSize.x, imageSize.y,
                0, GL_RGBA, GL_FLOAT, &correctedImage->at(0));
        } else {
            glTexSubImage2D(GL_TEXTURE_2D, 0,
                0, 0, imageSize.x, imageSize.y,
                GL_RGBA, GL_FLOAT, &correctedImage->at(0));
        }
        updateTextureRequired = false;
    }

    glColor4f(1,1,1,1);
    drawQuad(rasterOrigin, rasterCorner);

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void drawHud() {
    glViewport(0,viewSize.y,viewSize.x,getHudHeight());
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,viewSize.x, 0, getHudHeight(),-1,1);

    glColor4f(0,0,0,1);
    drawQuad(vec2i(0,0), vec2i(viewSize.x,getHudHeight()));

    char* pixelInfo = getPixelInfoString(false);
    char* imageInfo = getImageInfoString(false);
    glColor4f(1,1,1,1);
    if(hudMode == 1) {
        if(getStringLength(HUD_FONT, imageInfo) < viewSize.x) {
            glRasterPos2i(viewSize.x/2-getStringLength(HUD_FONT, imageInfo)/2,HUD_MARGIN);
        } else {
            glRasterPos2i(HUD_MARGIN,HUD_MARGIN);
        }
        drawString(HUD_FONT, imageInfo);
    } else if(hudMode == 2) {
        if(getStringLength(HUD_FONT, imageInfo) < viewSize.x && getStringLength(HUD_FONT, pixelInfo) < viewSize.x) {
            glRasterPos2i(viewSize.x/2-getStringLength(HUD_FONT, pixelInfo)/2,HUD_MARGIN);
        } else {
            glRasterPos2i(HUD_MARGIN,HUD_MARGIN);
        }
        drawString(HUD_FONT, pixelInfo);
        if(getStringLength(HUD_FONT, imageInfo) < viewSize.x && getStringLength(HUD_FONT, pixelInfo) < viewSize.x) {
            glRasterPos2i(viewSize.x/2-getStringLength(HUD_FONT, imageInfo)/2,2*HUD_MARGIN+HUD_LINEHEIGHT);
        } else {
            glRasterPos2i(HUD_MARGIN,2*HUD_MARGIN+HUD_LINEHEIGHT);
        }
        drawString(HUD_FONT, imageInfo);
    }
}

void display() {
    glClearColor(0.5,0.5,0.5,0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawView();
    if(hudMode != 0) drawHud();

    glutSwapBuffers();
}

void printHelp() {
    printf("------------ mouse\n");
    printf("left -> move image\n");
    printf("\n");

    printf("------------ keyboard\n");
    printf("space -> flip between images\n");
    printf("  [   -> increase exposure\n");
    printf("  ]   -> decrease exposure\n");
    printf("  e   -> input exposure\n");
    printf("  ;   -> increase gamma\n");
    printf("  \'   -> decrease gamma\n");
    printf("  g   -> input gamma\n");
    printf("  a   -> auto exposure with avg luminance\n");
    printf("  A   -> auto exposure with avg log luminance\n");
    printf("  1   -> set exposure to 1 and gamma to 1\n");
    printf("  2   -> set exposure to 1 and gamma to 2.2\n");
    printf("  0   -> set zoom to 1x\n");
    printf("  -   -> descrease zoom\n");
    printf("  =   -> increase zoom\n");
    printf("  f   -> zoom down to fit\n");
    printf("  i   -> switch title info mode\n");
    printf("  h   -> switch hud info mode\n");
    printf("  d   -> compute image difference\n");
    printf("  ?   -> print this message\n");
    printf("\n");
}

void reshape(int w, int h) {
    viewSize.x = w;
    viewSize.y = h - getHudHeight();

    if(isRasterSmaller()) center();

    glutPostRedisplay();
}

float getTypeValue(const char* message) {
    float ret;
    printf("%s", message);
    warning_if_not(scanf("%f", &ret) == 1, "no input value");
    return ret;
}

template<typename T>
sarray2<vec3<T> > ignoreAlpha(sarray2<vec4<T> > &im) {
    sarray2<vec3<T> > out(im.width(), im.height());
    for (int j = 0; j < im.height(); j++) {
        for (int i = 0; i < im.width(); i++) {
            const vec4<T> &v = im.at(i, j);
            out.at(i, j) = vec3<T>(v.x, v.y, v.z);
        }
    }
    return out;
}

void keyboard(unsigned char c, int x, int y) {
    switch(c) {
        case ' ':
            if(images.size() == 1) break;
            setCurrentImage((currentImageIdx+1)%images.size());
            break;
        case '[':
            exposureVal -= 1;
            computeCorrectedImage();
            break;
        case ']':
            exposureVal += 1;
            computeCorrectedImage();
            break;
        case 'e':
            exposureVal = getTypeValue("input exposure value: ");
            computeCorrectedImage();
            break;
        case ';':
            gammaVal -= 0.1f; if(gammaVal < 0.1f) gammaVal = 0.1f;
            computeCorrectedImage();
            break;
        case '\'':
            gammaVal += 0.1f;
            computeCorrectedImage();
            break;
        case 'g':
            gammaVal = getTypeValue("input gamma value: ");
            computeCorrectedImage();
            break;
        case 'a':
            computeAutoParams(true);
            computeCorrectedImage();
            break;
        case 'A':
            computeAutoParams(false);
            computeCorrectedImage();
            break;
        case '1':
            setExposureGamma11();
            computeCorrectedImage();
            break;
        case '2':
            setExposureGamma12();
            computeCorrectedImage();
            break;
        case '0':
            zoomOne();
            break;
        case '-':
            zoomDown();
            break;
        case '+':
        case '=':
            zoomUp();
            break;
        case 'f':
            zoomFit();
            break;
        case 'i':
            infoMode = (infoMode + 1) % MAX_INFOMODE;
            break;
        case 'h':
            hudMode = (hudMode + 1) % MAX_HUDMODE;
            reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
            break;
        case 'd':
            computeDifferenceImage();
            break;
        case 's':
            savePfm(currentName, ignoreAlpha(*currentImage));
            break;
        case '?':
            printHelp();
            break;
    }
    setTitle();
    glutPostRedisplay();
}


bool leftMouseDown = false;
bool rightMouseDown = false;
vec2i mouseBegin;
vec2i rasterOriginBegin;
bool moveRaster = false;
void mouse(int button, int state, int x, int y) {
    leftMouseDown = button == GLUT_LEFT_BUTTON && state == GLUT_DOWN;
    rightMouseDown = button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN;
    mouseBegin = vec2i(x,y);

    moveRaster = leftMouseDown && !isRasterSmaller();

    if(moveRaster) {
        rasterOriginBegin = rasterOrigin;
    }

    viewInspectionLocation = windowToView(vec2i(x,y));
    setTitle();
    glutPostRedisplay();
}

void motion(int x, int y) {
    vec2i delta = vec2i(x,y) - mouseBegin;

    if(moveRaster) {
        rasterOrigin = rasterOriginBegin + delta;
    }

    viewInspectionLocation = windowToView(vec2i(x,y));
    setTitle();
    glutPostRedisplay();
}

void passivemotion(int x, int y) {
    viewInspectionLocation = windowToView(vec2i(x,y));
    setTitle();
    glutPostRedisplay();
}

void initGui(int argc, char** argv) {
	glutInit(&argc, argv);

    viewSize = imageSize;
    vec2i windowSize = vec2i(imageSize.x,viewSize.y+getHudHeight());

	glutInitDisplayMode(GLUT_RGB | GLUT_ALPHA | GLUT_DOUBLE);
	glutInitWindowPosition(0,0);
    glutInitWindowSize(windowSize.x, windowSize.y);
    glutCreateWindow("extview");
	setTitle();
    glutSetCursor( GLUT_CURSOR_CROSSHAIR );

	// callbacks
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutPassiveMotionFunc(passivemotion);
    glutReshapeFunc(reshape);
}

void runGui() {
    glutMainLoop();
}

int main(int argc, char** argv) {
    vector<string> filenames;
    for(int i = 1; i < argc; i ++) {
        filenames.push_back(argv[i]);
    }
    if(filenames.size() == 0) {
        message("no inputs");
        return 0;
    }
    loadImages(filenames);
    computeCorrectedImage();

#ifdef  WIN32
    _set_output_format(_TWO_DIGIT_EXPONENT);
#endif

    initGui(argc, argv);
    runGui();
}
